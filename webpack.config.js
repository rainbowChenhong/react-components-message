/*
 * @Description:
 * @version: 1.0.0
 * @Author: chenhong
 * @Date: 2021-05-12 15:08:05
 * @LastEditors: chenhong
 * @LastEditTime: 2021-05-12 16:20:15
 */
const path = require("path");
const webpack = require("webpack");

module.exports = {
  entry: "./src/index.js",
  mode: "development",
  resolve: {extensions: [".ts", ".tsx", "*", ".js", ".jsx"]},
  module: {
    rules: [
      // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
      {test: /\.tsx?$/, loader: "awesome-typescript-loader"},

      // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
      // {enforce: "pre", test: /\.js$/, loader: "source-map-loader"},
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: "babel-loader",
        options: {presets: ["@babel/env"]},
      },
      {
        test: /\.css$|\.less$/,
        use: ["style-loader", "css-loader", "less-loader"],
      },
    ],
  },

  output: {
    path: path.resolve(__dirname, "dist/"),
    publicPath: "/dist/",
    filename: "bundle.js",
  },
  devServer: {
    contentBase: path.join(__dirname, "public/"),
    port: 3000,
    publicPath: "http://localhost:3000/dist/",
    hotOnly: true,
  },
  plugins: [new webpack.HotModuleReplacementPlugin()],
};
