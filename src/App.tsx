/*
 * @Description:
 * @version: 1.0.0
 * @Author: chenhong
 * @Date: 2021-05-12 15:09:20
 * @LastEditors: chenhong
 * @LastEditTime: 2021-05-15 14:04:46
 */
import * as React from "react";
import Message from "./MessageContianer";
let notification: any = null;
let MessageInstance = Message.newInstance(
  {
    maxCount: 5,
  },
  (n: any) => {
    notification = n;
  }
);
const clearPath =
  "M793 242H366v-74c0-6.7-7.7-10.4-12.9" +
  "-6.3l-142 112c-4.1 3.2-4.1 9.4 0 12.6l142 112c" +
  "5.2 4.1 12.9 0.4 12.9-6.3v-74h415v470H175c-4.4" +
  " 0-8 3.6-8 8v60c0 4.4 3.6 8 8 8h618c35.3 0 64-" +
  "28.7 64-64V306c0-35.3-28.7-64-64-64z";

const getSvg = (path: string, props = {}, align = false) => (
  <i {...props}>
    <svg
      viewBox="0 0 1024 1024"
      width="1em"
      height="1em"
      fill="currentColor"
      style={align ? {verticalAlign: "-.125em "} : {}}
    >
      <path d={path} />
    </svg>
  </i>
);
class App extends React.Component {
  addMessage = () => {
    notification.notice({
      id: Date.now(),
      duration: 7,
      text: <div>点击点击</div>,
      onClick: () => {
        console.log("点击");
      },
      closable: true,
      closeIcon: getSvg(clearPath, {}, true),
      className: "className",
    });
  };
  render() {
    return (
      <div className="App">
        <h1> Hello, World! </h1>
        <button onClick={this.addMessage}>弹出信息</button>
      </div>
    );
  }
}

export default App;
