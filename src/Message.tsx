/*
 * @Description:
 * @version: 1.0.0
 * @Author: chenhong
 * @Date: 2021-05-12 16:43:13
 * @LastEditors: chenhong
 * @LastEditTime: 2021-05-15 13:58:19
 */
import classNames from "classnames";
import * as React from "react";
export interface MessageProps {
  children: React.ReactNode;
  duration?: number | null;
  messagekey: React.Key;
  onClose: (key: React.Key) => void;
  prefixCls?: string;
  closeIcon?: React.ReactNode;
  closable?: boolean;
  onClick?: React.MouseEventHandler<HTMLDivElement>;
  className?: string;
}
function Message(props: MessageProps) {
  const {
    children,
    duration = 1.5,
    onClose,
    messagekey,
    prefixCls,
    closeIcon,
    closable = false,
    onClick,
    className,
  } = props;
  let closeTimer: number | null = null;
  const componentClass = `${prefixCls}-mess`;
  console.log("render,", messagekey);
  const startCloseTimer = () => {
    closeTimer = window.setTimeout(() => {
      close();
    }, duration * 1000);
  };
  React.useEffect(() => {
    startCloseTimer();
  }, []);
  const close = (e?: React.MouseEvent<HTMLAnchorElement>) => {
    if (e) {
      e.stopPropagation();
    }
    clearCloseTimer();
    if (onClose) {
      onClose(messagekey);
    }
  };
  const clearCloseTimer = () => {
    if (closeTimer) {
      clearTimeout(closeTimer);
      closeTimer = null;
    }
  };

  return (
    <div
      className={classNames(
        componentClass,
        className,
        closable ? `${componentClass}-closable` : null
      )}
      onMouseEnter={clearCloseTimer}
      onMouseLeave={startCloseTimer}
      onClick={onClick}
    >
      <div>
        {children}
        {messagekey}
      </div>
      {closable && (
        <a tabIndex={0} onClick={close} className={`${componentClass}-close`}>
          {closeIcon || <span className={`${componentClass}-close-x`} />}
        </a>
      )}
    </div>
  );
}
// const _getKeyValue_ = (key: string) => (obj: Record<string, any>) => obj[key];
// function areEqual(prevProps: MessageProps, nextProps: MessageProps) {
//   if (Object.keys(prevProps) >= Object.keys(nextProps)) {
//     let keys: Array<string> = Object.keys(prevProps);
//     for (let key of keys) {
//       if (_getKeyValue_(key)(prevProps) !== _getKeyValue_(key)(nextProps)) {
//         return true;
//       }
//     }
//     return false;
//   } else {
//     return true;
//   }
// }
// const MyComponent = React.memo(Message);
export default Message;
