/*
 * @Description:
 * @version: 1.0.0
 * @Author: chenhong
 * @Date: 2021-05-12 16:04:23
 * @LastEditors: chenhong
 * @LastEditTime: 2021-05-29 14:20:42
 */
import * as React from "react";
import * as ReactDOM from "react-dom";
import classNames from "classnames";
import "./../assets/index.less";
import Message, {MessageProps} from "./Message";

export interface MessageContainerProps {
  prefixCls?: string;
  className?: string;
  style?: React.CSSProperties;
  transitionName?: string;
  animation?: string | object;
  maxCount?: number;
  closeIcon?: React.ReactNode;
  // register: Function;
}
let seed = 0;
const now = Date.now();
function getUuid() {
  const id = seed;
  seed += 1;
  return `rc-message_${now}_${id}`;
}
// function MessageContianer(props: MessageContainerProps) {
//   const {
//     prefixCls = "rc-message",
//     className,
//     closeIcon,
//     style = {
//       top: 65,
//       left: "50%",
//     },
//     register,
//   } = props;

//   const [messages, setMessages] = React.useState([]);

//   function close(key: React.Key) {
//     let index = messages.findIndex((item) => item.id === key);
//     if (index >= 0) {
//       messages.splice(index, 1);
//       setMessages([...messages]);
//     }
//   }
//   function add(originNotice: MessageProps) {
//     const key = originNotice.messagekey || getUuid();
//     const notice = {
//       ...originNotice,
//       messagekey: key,
//       onClose: (messagekey: React.Key) => {
//         close(messagekey);
//         originNotice.onClose?.();
//       },
//     };
//     messages.push(notice);
//     setMessages([...messages]);
//   }
//   React.useEffect(() => {
//     register({
//       notice: add,
//       remove: close,
//     });
//   });
//   return (
//     <div className={classNames(prefixCls, className)} style={style}>
//       <div>
//         {messages.map((item) => {
//           return (
//             <Message
//               {...item}
//               messagekey={item.id}
//               key={item.id}
//               prefixCls={prefixCls}
//               duration={item.duration}
//               closable={item.closable}
//               closeIcon={item.closeIcon}
//               onClick={item.onClick}
//             >
//               <div>{item.text}</div>
//             </Message>
//           );
//         })}
//       </div>
//     </div>
//   );
// }
export interface MessagePropsContent
  extends Omit<MessageProps, "prefixCls" | "onClose" | "onClick"> {
  prefixCls?: string;
  id?: React.Key;
  text?: React.ReactNode;
  onClose?: (key: React.Key) => void;
}
export type MessageFunc = (noticeProps: MessageProps) => void;
export interface NotificationInstance {
  notice: MessageFunc;
  remove: (key: React.Key) => void;
  destroy: () => void;
  component: MessageContianer;
  useNotification: () => [MessageFunc, React.ReactElement];
}
interface MessageContainerState {
  messages: Array<MessageProps>;
}
class MessageContianer extends React.Component<
  MessageContainerProps,
  MessageContainerState
> {
  static newInstance: (
    properties: MessageContainerProps & {getContainer?: () => HTMLElement},
    callback: (instance: NotificationInstance) => void
  ) => void;
  static defaultProps = {
    prefixCls: "rc-message",
    animation: "fade",
    style: {
      top: 65,
      left: "50%",
    },
  };
  constructor(props: MessageContainerProps) {
    super(props);
    this.state = {
      messages: [],
    };
  }
  componentDidMount() {}
  add = (originNotice: MessagePropsContent) => {
    this.setState(({messages}) => {
      const key = originNotice.messagekey || getUuid();
      let message: MessageProps = {
        ...originNotice,
        messagekey: key,
        prefixCls: this.props.prefixCls,
        children: originNotice.text,
        onClose: (key: React.Key) => {
          this.remove(key);
          originNotice.onClose?.(key);
        },
      };
      messages.push(message);
      return {
        messages,
      };
    });
  };
  remove = (key: React.Key) => {
    this.setState(({messages}) => {
      let index = messages.findIndex((item) => item.messagekey === key);
      if (index >= 0) {
        messages.splice(index, 1);
      }
      return {
        messages,
      };
    });
  };
  render() {
    const {prefixCls, className, style} = this.props;
    const {messages} = this.state;
    console.log(messages, "messages");

    return (
      <div className={classNames(prefixCls, className)} style={style}>
        {messages.map((item) => (
          <Message {...item} key={item.messagekey}></Message>
        ))}
      </div>
    );
  }
}
export interface propertiesProps {
  [propName: string]: any;
}

MessageContianer.newInstance = function newMessageContianer(
  properties: propertiesProps,
  callback: Function
) {
  const {getContainer, ...props} = properties || {};
  const div = document.createElement("div");
  if (getContainer) {
    const root = getContainer();
    root.appendChild(div);
  } else {
    document.body.appendChild(div);
  }
  // function ref(data: Object) {
  //   callback({
  //     ...data,
  //     destroy() {
  //       ReactDOM.unmountComponentAtNode(div);
  //       if (div.parentNode) {
  //         div.parentNode.removeChild(div);
  //       }
  //     },
  //   });
  // }
  function ref(messageContianer: MessageContianer) {
    callback({
      notice: messageContianer.add,
      remove: messageContianer.remove,
      omponent: messageContianer,
      destroy() {
        ReactDOM.unmountComponentAtNode(div);
        if (div.parentNode) {
          div.parentNode.removeChild(div);
        }
      },
    });
  }

  ReactDOM.render(<MessageContianer {...props} ref={ref} />, div);
};

export default MessageContianer;
