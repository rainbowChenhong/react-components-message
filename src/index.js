/*
 * @Description:
 * @version: 1.0.0
 * @Author: chenhong
 * @Date: 2021-05-12 15:01:19
 * @LastEditors: chenhong
 * @LastEditTime: 2021-05-12 15:51:55
 */
import React from "react";
import ReactDOM from "react-dom";
import App from "./App.tsx";
ReactDOM.render(<App />, document.getElementById("root"));
